# **********************************************************************
#
# Copyright (c) 2003-2014 ZeroC, Inc. All rights reserved.
#
# This copy of Ice is licensed to you under the terms described in the
# ICE_LICENSE file included in this distribution.
#
# **********************************************************************

#
# Define OPTIMIZE as yes if you want to build with
# optimization. Otherwise Ice is build with debug information.
#
#OPTIMIZE		= yes

#
# Define LP64 as yes or no if you want force a 32 or 64 bit. The
# default is platform-dependent
#
#LP64			?= yes

#
# The "root directory" for runpath embedded in executables. Can be set
# to change the runpath added to Ice executables. The default is 
# platform dependent.
#
#embedded_runpath_prefix ?= /opt/Ice-$(VERSION_MAJOR).$(VERSION_MINOR)

#
# Define embedded_runpath as no if you don't want any RPATH added to
# the executables.
#
embedded_runpath ?= yes

#
# If building on OS X and third party libraries are not installed
# in the default location and THIRDPARTY_HOME is not set in your
# environment variables, change the following setting to reflect the
# installation location.
#
#THIRDPARTY_HOME ?= /Library/Developer/Ice-$(VERSION)-ThirdParty

#
# If libbzip2 is not installed in a location where the compiler can
# find it, set BZIP2_HOME to the bzip2 installation directory.
#
#BZIP2_HOME		?= /opt/bzip2

#
# If Berkeley DB is not installed in a location where the compiler can
# find it, set DB_HOME to the Berkeley DB installation directory.
#
#DB_HOME		?= /opt/db

#
# Define CPP11 as yes if you want to enable C++11 features in GCC or 
# Clang.
#
#CPP11     ?= yes

# ----------------------------------------------------------------------
# Don't change anything below this line!
# ----------------------------------------------------------------------

#
# Common definitions
#
ice_language     = cpp
slice_translator = slice2cpp
ice_require_cpp  = 1

include $(top_srcdir)/config/Make.common.rules

includedir		= $(ice_dir)/include

#
# Platform specific definitions
#
include	 $(top_srcdir)/config/Make.rules.$(UNAME)

ICECPPFLAGS		= -I$(slicedir)

SLICE2CPPFLAGS		= $(ICECPPFLAGS)

ifeq ($(ice_dir), /usr) 
    LDFLAGS	= $(LDPLATFORMFLAGS) $(CXXFLAGS)
    ifeq ($(CPP11),yes)
        LDFLAGS = $(LDPLATFORMFLAGS) $(CXXFLAGS) -L$(ice_dir)/$(libsubdir)$(cpp11libdirsuffix)
    endif
else
    CPPFLAGS	+= -I$(includedir)
    LDFLAGS	= $(LDPLATFORMFLAGS) $(CXXFLAGS) -L$(ice_dir)/$(libsubdir)$(cpp11libdirsuffix)
endif

ifeq ($(FLEX_NOLINE),yes)
    FLEXFLAGS	       := -L
else
    FLEXFLAGS	       :=
endif

ifeq ($(BISON_NOLINE),yes)
    BISONFLAGS		:= -dvtl
else
    BISONFLAGS		:= -dvt
endif

ifeq ($(mkshlib),)
    $(error You need to define mkshlib in Make.rules.$(UNAME)) 
endif


SLICEPARSERLIB  = $(ice_dir)/$(libsubdir)/$(subst $(cpp11libsuffix),,$(call mklibfilename,Slice,$(VERSION)))
ifeq ($(wildcard $(SLICEPARSERLIB)),)
	SLICEPARSERLIB  = $(ice_dir)/$(lib64subdir)/$(subst $(cpp11libsuffix),,$(call mklibfilename,Slice,$(VERSION)))
endif
SLICE2CPP	= $(ice_dir)/$(binsubdir)/slice2cpp
SLICE2FREEZE	= $(ice_dir)/$(binsubdir)/slice2freeze

EVERYTHING		= all clean
EVERYTHING_EXCEPT_ALL   = clean

.SUFFIXES:
.SUFFIXES:		.cpp .c .o

.cpp.o:
	$(CXX) -c $(CPPFLAGS) $(CXXFLAGS) $<

.c.o:
	$(CC) -c $(CPPFLAGS) $(CFLAGS) $<

%.h %.cpp: %.ice $(SLICE2CPP) $(SLICEPARSERLIB)
	rm -f $(*F).h $(*F).cpp
	$(SLICE2CPP) $(SLICE2CPPFLAGS) $(*F).ice	

%.h %.cpp: %.y
	rm -f $(*F).h $(*F).cpp
	bison $(BISONFLAGS) $<
	mv $(*F).tab.c $(*F).cpp
	mv $(*F).tab.h $(*F).h
	rm -f $(*F).output

%.cpp: %.l
	flex $(FLEXFLAGS) $<
	rm -f $@
	echo '#include <IceUtil/Config.h>' > $@
	cat lex.yy.c >> $@
	rm -f lex.yy.c

all:: $(SRCS) $(TARGETS)

clean::
	-rm -f $(TARGETS)
	-rm -f core *.o *.bak

ifneq ($(SLICE_SRCS),)
clean::
	rm -f $(addsuffix .cpp, $(basename $(notdir $(SLICE_SRCS))))
	rm -f $(addsuffix .h, $(basename $(notdir $(SLICE_SRCS))))
endif
